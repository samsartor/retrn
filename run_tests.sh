#!/usr/bin/env bash

set -e

# Some tests rely on exclusive access to the schedular
cargo test --locked --lib -- finalize --test-threads 1
# Basic & reactivity tests should be more robust
cargo test --locked --lib -- reactivity basic
cargo test --locked --release --lib -- reactivity basic
