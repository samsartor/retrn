# Hornpipe

<span style="color:red; font-size=1.5em;">Hornpipe is still in early development! The API is constantly changing,
so many examples may not compile or run correctly. If you want to help out, please message me on Matrix. In particular,
I need help writing unit and integrations tests. Probably we need a lot of prop testing and fuzzing before release.</span>

Hornpipe is a reactivity system! While Rust carefully constrains mutability at compile-time,
Hornpipe intelligently traces mutations at run-time.

```rust
hornpipe_extern! {
    struct App {
        mut count: i32,
        computed double_count: i32,
    }
}

impl App {
    fn double_count(this: TxRef<Self>) -> i32 {
        this.count().get() * 2
    }
}

let app = App::new(0);

{
    let tx = Transaction::new();
    let app = app.attach(&tx);
    app.count().set(10);
    assert_eq!(app.double_count.get(), 20);
    tx.commit();
}
```

Hornpipe also implements a weak reference system to help users escape the borrow checker:
```rust
let app = app.refer();
let callback: Box<dyn Fn(usize) + Send + Sync + 'static> = Box::new(|amount| {
    let tx = Transaction::new();
    let Some(app) = app.attach(&tx) else { return };
    app.count().set(app.count().get() + amount);
    tx.commit();
});
```

Although Hornpipe is designed as a general-purpose state management framework, it integrates very
nicely with GUI libraries.

```rust
// Using Dioxus
// ============
fn app(cx: Scope) -> Element {
    let root = use_state(&cx, || App::new(0));

    use_hornpipe(&cx, |tx| {
        let root = root.attach(&tx);
        let count = root.count();
        cx.render(rsx! {
            [ format!("{}×2 = {}", count.get(), root.double_count().get()) ]
            button {
                onclick: move |_| count.detached.operate(|c| c.set(c.get() + 1)),
                "Count Up",
            }
            button {
                onclick: move |_| count.detached.operate(|c| c.set(c.get() - 1)),
                "Count Down",
            }
        })
    })
}

// Using Xilem
// ===========
fn app(root: Ref<App>) -> impl View<Ref<App>> {
    Reactor::new(|tx| {
        let root = root.attach(&tx);
        let count = root.count();
        h_stack((
            text(format!("{}×2 = {}", count.get(), root.double_count().get())),
            button(
                "Count Up",
                move |_| count.detached.operate(|c| c.set(c.get() + 1)),
            ),
            button(
                "Count Down",
                move |_| count.detached.operate(|c| c.set(c.get() - 1)),
            ),
        ))
    })
} 
```


More examples can be found in `/gui/examples`. The `/interface` directory provides a nice API
for constructing objects, passing references, and mutating data at-will. However, it is less
like a pure Rust library and more like an FFI wrapper to the runtime in `/core`, which can be
thought of as a whole programming language minus the syntax.
