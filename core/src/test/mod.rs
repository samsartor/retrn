// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::transaction::hornpipedbg_lookup_transaction_copy;
use crate::{self as hornpipe_core, Ref, TxRef, Universe};
use crate::{data::Pod, declare_universe, pin, Counter, Data, Own, Register, Transaction};

#[test]
fn test_dbg_exports() {
    declare_universe!(Test);

    let a = Transaction::start_in(Test);
    let x: Own<Register<Pod<i32, Test>>> = Own::new(Register::new(Pod::new(42)));
    let x = x.refer();

    let x: crate::TxRef<'_, Register<Pod<i32, Test>>> = x.attach(&a).unwrap();
    x.write(Pod::new(69));

    unsafe {
        crate::transaction::hornpipedbg_lookup_transaction_copy(
            Ref::as_raw(x.tx).0,
            Ref::as_raw(x.detach).0,
        );
    }
}

#[test]
fn test_simple_owned_value() {
    let x: Own<i32> = Own::new(42);
    let x = x.refer();
    {
        let guard = pin();
        let x_value = x.load(&guard).expect("not dropped");
        assert_eq!(x_value, &42);
    }
}

#[test]
fn test_txref_deref() {
    declare_universe!(Test);

    #[derive(Data)]
    #[universe = "Test"]
    struct State(Pod<i32, Test>, Pod<i32, Test>);

    let x: Own<State> = Own::new(State(Pod::new(42), Pod::new(24)));

    let a = Transaction::start_in(Test);
    let x = x.refer().attach(&a).unwrap();
    assert_eq!(**x.get0(), 42);
    assert_eq!(**x.get1(), 24);
}

#[test]
fn test_temporary_register_write() {
    declare_universe!(Test);

    let x: Own<Register<Pod<i32, Test>>> = Own::new(Register::new(Pod::new(42)));
    let x = x.refer();

    let a = Transaction::start_in(Test);
    let x = x.attach(&a).unwrap();
    assert_eq!(*x.read(), 42);
    x.write(Pod::new(69));
    assert_eq!(*x.read(), 69);
}

#[test]
fn test_committed_register_write() {
    declare_universe!(Test);

    let x: Own<Register<Pod<i32, Test>>> = Own::new(Register::new(Pod::new(42)));
    let x = x.refer();

    {
        let a = Transaction::start_in(Test);
        let x: crate::TxRef<'_, Register<Pod<i32, Test>>> = x.attach(&a).unwrap();
        x.write(Pod::new(69));
        a.commit();
    }

    {
        let b = Transaction::start_in(Test);
        let x = x.attach(&b).unwrap();
        assert_eq!(*x.read(), 69);
        b.commit();
    }
}

#[test]
fn test_isolated_register_write() {
    declare_universe!(Test);

    let x: Own<Register<Pod<i32, Test>>> = Own::new(Register::new(Pod::new(42)));
    let x = x.refer();

    let a = Transaction::start_in(Test);
    let b = Transaction::start_in(Test);

    {
        let x: crate::TxRef<'_, Register<Pod<i32, Test>>> = x.attach(&a).unwrap();
        x.write(Pod::new(69));
        a.commit();
    }

    {
        let x = x.attach(&b).unwrap();
        assert_eq!(*x.read(), 42);
        b.commit();
    }
}

#[test]
fn test_temporary_counter_write() {
    dbg!(hornpipedbg_lookup_transaction_copy as usize);
    declare_universe!(Test);

    let x: Own<Counter<Pod<i32, Test>>> = Own::new(Counter::new(Pod::new(42)));
    let x = x.refer();

    let a = Transaction::start_in(Test);
    let mut x = x.attach(&a).unwrap();
    assert_eq!(*x.read(), 42);
    let y = x.detach;
    x += Pod::new(1);
    assert_eq!(*x.read(), 43);
}

#[test]
fn test_committed_counter_write() {
    declare_universe!(Test);
    crate::tracing_on();

    let x: Own<Counter<Pod<i32, Test>>> = Own::new(Counter::new(Pod::new(42)));
    let x = x.refer();

    {
        let a = Transaction::start_in(Test);
        let mut x: crate::TxRef<'_, Counter<Pod<i32, Test>>> = x.attach(&a).unwrap();
        x += Pod::new(1);
        a.commit();
    }

    {
        let b = Transaction::start_in(Test);
        let x = x.attach(&b).unwrap();
        assert_eq!(*x.read(), 43);
        b.commit();
    }
}

#[test]
fn test_isolated_counter_write() {
    declare_universe!(Test);

    let x: Own<Counter<Pod<i32, Test>>> = Own::new(Counter::new(Pod::new(42)));
    let x = x.refer();

    let a = Transaction::start_in(Test);
    let b = Transaction::start_in(Test);

    {
        let mut x: crate::TxRef<'_, Counter<Pod<i32, Test>>> = x.attach(&a).unwrap();
        x += Pod::new(1);
        a.commit();
    }

    {
        let x = x.attach(&b).unwrap();
        assert_eq!(*x.read(), 42);
    }
}

#[test]
fn test_concurrent_counter_write() {
    declare_universe!(Test);

    let x: Own<Counter<Pod<i32, Test>>> = Own::new(Counter::new(Pod::new(42)));
    let x = x.refer();

    let a = Transaction::start_in(Test);
    let b = Transaction::start_in(Test);

    {
        let mut x: crate::TxRef<'_, Counter<Pod<i32, Test>>> = x.attach(&a).unwrap();
        x += Pod::new(1);
        assert_eq!(*x.read(), 43);
        a.commit();
    }

    {
        let mut x = x.attach(&b).unwrap();
        x += Pod::new(1);
        assert_eq!(*x.read(), 43);
        b.commit();
    }

    {
        let c = Transaction::start_in(Test);
        let x = x.attach(&c).unwrap();
        assert_eq!(*x.read(), 44);
    }
}
