use crate as hornpipe_core;
use crate::framing::DataPtr;
use crate::refs::TxRef;
use crate::share::OwnershipFlagged;
use crate::share::PartiallyOwned;
use crate::transaction::{LifecycleContext, TransactionStepRef};
use crate::{Data, Universe};
use hornpipe_macros::{CopyRef, ProjectPartiallyOwned};
use std::mem::replace;
use std::ops::{Add, AddAssign, Neg, SubAssign};

#[derive(ProjectPartiallyOwned, CopyRef)]
pub struct Register<T: Data> {
    value: OwnershipFlagged<T>,
    last_modified: TransactionStepRef<T::Universe>,
}

impl<T: Data> Register<T> {
    pub const fn new(value: T) -> Self {
        Register {
            value: OwnershipFlagged::new_owned(value),
            last_modified: TransactionStepRef::eternal(),
        }
    }
}

impl<'tx, T: Data> TxRef<'tx, Register<T>> {
    pub fn read(&self) -> T
    where
        T: Clone,
    {
        self.read_then(T::clone)
    }

    pub fn read_then<O>(&self, func: impl FnOnce(&T) -> O) -> O {
        let read = T::Universe::handle().read();
        let data = TxRef::load_tx(self);
        let state = data.state(&read);
        let mut state = state.borrow_mut();
        let this = state
            .get_ref(TxRef::guard(self), self.detach)
            .expect("null inside TxRef<Register<_>>");
        func(this.value_ref().get_ref())
    }

    pub fn write(&self, value: T) {
        let read = T::Universe::handle().read();
        let state = TxRef::load_tx(self).state(&read);
        let mut state = state.borrow_mut();
        let step = TransactionStepRef {
            tx: self.tx,
            step: state.next_step(),
        };
        let this = state
            .make_mut(TxRef::guard(self), self.detach)
            .expect("null inside TxRef<Register<_>>");
        **this.last_modified_mut() = step;
        drop(this.value_mut().replace_with_owned(value));
    }

    pub fn write_then<O>(&self, func: impl FnOnce(&mut T) -> O) -> O
    where
        T: Clone,
    {
        let read = T::Universe::handle().read();
        let state = TxRef::load_tx(self).state(&read);
        let mut state = state.borrow_mut();
        let step = TransactionStepRef {
            tx: self.tx,
            step: state.next_step(),
        };
        let this = state
            .make_mut(TxRef::guard(self), self.detach)
            .expect("null inside TxRef<Register<_>>");
        **this.last_modified_mut() = step;
        func(this.value_mut().get_mut_or_clone(T::clone))
    }
}

// SAFETY: The indirection always owns each register, otherwise
// registers are owned by whatever transaction is in the `owner` field.
// Only that transaction is allowed to drop the contents of the register,
// and even then only through the `LifecycleContext` since other transactions
// might still be accessing the content concurrently.
impl<T: Data> Data for Register<T> {
    type Universe = T::Universe;

    fn finalize<'temp, 'global: 'temp>(
        internal: &'temp mut PartiallyOwned<'temp, Register<T>>,
        outgoing: &'temp mut PartiallyOwned<'global, Register<T>>,
        ctx: LifecycleContext<<Register<T> as Data>::Universe>,
    ) {
        if **outgoing.last_modified_ref() >= **internal.last_modified_ref() {
            // Outgoing is the last write.
            *internal = PartiallyOwned::copy(&**outgoing);
        } else {
            // Internal is the last write, the outgoing value should be overwritten.
            internal
                .value_mut()
                .swap_owned(outgoing.value_mut())
                .unwrap();
            **internal.last_modified_mut() = ctx.step;
        }
    }
}

#[derive(Default, ProjectPartiallyOwned, CopyRef)]
pub struct Counter<T: Data> {
    diff: OwnershipFlagged<T>,
    base_count: OwnershipFlagged<T>,
}

impl<T> Data for Counter<T>
where
    T: Data + Add<Output = T> + Neg<Output = T> + Default + Clone,
{
    type Universe = T::Universe;

    fn finalize<'temp, 'global: 'temp>(
        internal: &'temp mut PartiallyOwned<'temp, Counter<T>>,
        outgoing: &'temp mut PartiallyOwned<'global, Counter<T>>,
        _ctx: LifecycleContext<<Counter<T> as Data>::Universe>,
    ) {
        // The internal value should have the negative of this diff, so that it can undo this operation
        let undo_diff = -internal.diff_ref().get_ref().clone();
        if let Some(our_diff) = internal.diff_mut().replace_with_owned(undo_diff) {
            // Calculate a new base count for the outgoing counter and our diff
            let new_base_count = (*outgoing.base_count).clone() + our_diff;
            // Replace the outgoing base count with the newly calculated value.
            // Move the previous base count into internal although it isn't needed
            // for undo, because we can't drop it here.
            *internal.base_count_mut() = replace(
                outgoing.base_count_mut(),
                OwnershipFlagged::new(new_base_count),
            );
        } else {
            // Nothing to do if this transaction did not create a new diff.
        }
    }
}

impl<T: Data + Default> Counter<T> {
    pub fn new(value: T) -> Self {
        Counter {
            base_count: OwnershipFlagged::new_owned(value),
            diff: OwnershipFlagged::<T>::default(),
        }
    }
}

impl<'tx, T> TxRef<'tx, Counter<T>>
where
    T: Data + Add<Output = T> + Neg<Output = T> + Default + Clone,
{
    pub fn read(&self) -> T {
        let read = T::Universe::handle().read();
        let state = TxRef::load_tx(self).state(&read);
        let mut state = state.borrow_mut();
        let this = state
            .get_ref(TxRef::guard(self), self.detach)
            .expect("null inside TxRef<Counter<_>>");
        this.base_count_ref().get_ref().clone() + this.diff_ref().get_ref().clone()
    }

    pub fn increment(&self, by: T) -> T {
        let read = T::Universe::handle().read();
        let state = TxRef::load_tx(self).state(&read);
        let mut state = state.borrow_mut();
        let this = state
            .make_mut(TxRef::guard(self), self.detach)
            .expect("null inside TxRef<Counter<_>>");
        let base_count = this.base_count_ref().get_ref().clone();
        let diff = this.diff_mut().get_mut_or_clone(T::clone);
        *diff = diff.clone() + by;
        base_count + diff.clone()
    }

    fn increment_fast(&self, by: T) {
        let read = T::Universe::handle().read();
        let state = TxRef::load_tx(self).state(&read);
        let mut state = state.borrow_mut();
        let this = state
            .make_mut(TxRef::guard(self), self.detach)
            .expect("null inside TxRef<Counter<_>>");
        let diff = this.diff_mut().get_mut_or_clone(T::clone);
        *diff = diff.clone() + by;
    }
}

impl<'tx, T> AddAssign<T> for TxRef<'tx, Counter<T>>
where
    T: Data + Add<Output = T> + Neg<Output = T> + Default + Clone,
{
    fn add_assign(&mut self, rhs: T) {
        self.increment_fast(rhs);
    }
}

impl<'tx, T> SubAssign<T> for TxRef<'tx, Counter<T>>
where
    T: Data + Add<Output = T> + Neg<Output = T> + Default + Clone,
{
    fn sub_assign(&mut self, rhs: T) {
        self.increment_fast(-rhs);
    }
}
