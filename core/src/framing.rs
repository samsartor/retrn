// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Responsible for allocating data on the heap and checking pointers thereof.

use crate as hornpipe_core;
use crate::data::{data_operations, Operations};
use crate::share::CopyRef;
use crate::share::PartiallyOwned;
use crate::transaction::LifecycleContext;
use crate::universe::Recycler;
use crate::{Data, Universe};
use crossbeam_epoch::{pin, Guard};
#[cfg(feature = "labels")]
use crossbeam_epoch::{Atomic, Owned, Shared};
use hornpipe_macros::ProjectPartiallyOwned;
use std::alloc::Layout;
use std::any::type_name;
use std::ffi::c_void;
use std::mem::{align_of, size_of, transmute};
use std::ptr;
use std::sync::atomic::{AtomicPtr, AtomicU64, AtomicUsize, Ordering};
use std::{cmp, fmt, hash};

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Metadata {
    pub operations: Operations,
    /// The size of this allocation.
    pub frame_size: u32,
    /// The alignment of this allocation.
    pub frame_align: u32,
}

impl Metadata {
    pub const fn new<T: Data>() -> Self {
        Metadata {
            operations: data_operations::<T>(),
            frame_size: match size_of::<DataFraming<T>>() {
                x if x < u32::MAX as usize => x as u32,
                _ => panic!("size is too large"),
            },
            frame_align: match align_of::<DataFraming<T>>() {
                x if x < u32::MAX as usize => x as u32,
                _ => panic!("alignment is too large"),
            },
        }
    }

    pub fn frame_layout(&self) -> Layout {
        match Layout::from_size_align(self.frame_size as _, self.frame_align as _) {
            Ok(l) => l,
            Err(err) => panic!("metadata specified invalid layout: {err:?}"),
        }
    }
}

#[derive(CopyRef)]
pub struct UnknownData;

impl UnknownData {
    /// SAFETY: the user must ensure that `self + offset` is actually a reference to an instance of T
    #[inline]
    pub unsafe fn to_known<T: Data>(&self, offset: isize) -> &T {
        &*(self as *const UnknownData)
            .cast::<u8>()
            .offset(offset)
            .cast::<T>()
    }

    /// SAFETY: the user must ensure that `self + offset` is actually a reference to an instance of T
    #[inline]
    pub unsafe fn to_known_mut<T: Data>(&mut self, offset: isize) -> &mut T {
        &mut *(self as *mut UnknownData)
            .cast::<u8>()
            .offset(offset)
            .cast::<T>()
    }
}

impl<'a> PartiallyOwned<'a, UnknownData> {
    /// SAFETY: the user must ensure that `self + offset` is actually a reference to an instance of T
    #[inline]
    pub unsafe fn to_known_partially_owned<T: Data>(
        &mut self,
        offset: isize,
    ) -> &mut PartiallyOwned<'a, T> {
        transmute(transmute::<_, *const c_void>(self).offset(offset))
    }
}

#[cfg(debug_assertions)]
type GDBHint = [u8; 16];
#[cfg(not(debug_assertions))]
type GDBHint = ();

/*
/// This only exists to make sure the DataFraming type has debug symbols exported.
/// LLDB 18 doesn't seem to care but LLDB 17 does? Figure that out.
#[export_name = "BLACK_BOX_DATAFRAMING"]
static BLACK_BOX_DATAFRAMING: DataFraming<UnknownData> = DataFraming {
    meta: Metadata::new::<()>(),
    delete: |_| (),
    users: AtomicUsize::new(0),
    gdb_hint: *b"BLACK_BOX_DATAFR",
    data: UnknownData,
};
*/

#[derive(ProjectPartiallyOwned, CopyRef)]
#[repr(C)]
pub struct DataFraming<T: CopyRef> {
    pub meta: Metadata,
    pub delete: unsafe fn(*mut DataFraming<UnknownData>),
    /// The number of transactions (or other users) accessing this allocation
    // SAFETY: we want each shallow copy to have its own atomic count
    #[unsafe_skip]
    pub users: AtomicUsize,
    /// Helps us spot this instance in GDB.
    gdb_hint: GDBHint,
    pub data: T,
}

impl<T: Data> DataFraming<T> {
    #[cfg(debug_assertions)]
    fn gdb_hint() -> GDBHint {
        let mut bytes = [0; 16];
        let name = type_name::<T>();
        let name = name.trim_start_matches("std::");
        let name = name.trim_start_matches("core::");
        let name = name.trim_start_matches("hornpipe_core::");
        if bytes.len() < name.len() {
            let name = &name.as_bytes()[..bytes.len()];
            bytes.copy_from_slice(name);
        } else {
            bytes[..name.len()].copy_from_slice(name.as_bytes());
        }
        bytes
    }

    #[cfg(debug_assertions)]
    const fn const_gdb_hint() -> GDBHint {
        [0; 16]
    }

    #[cfg(not(debug_assertions))]
    fn gdb_hint() -> GDBHint {}

    #[cfg(not(debug_assertions))]
    const fn const_gdb_hint() -> GDBHint {}

    pub fn new_boxed(data: T) -> Box<Self> {
        Box::new(DataFraming {
            meta: dbg!(Metadata::new::<T>()),
            delete: |ptr| {
                drop(unsafe { Self::known_box(ptr) });
            },
            users: AtomicUsize::new(1),
            gdb_hint: Self::gdb_hint(),
            data,
        })
    }

    pub const fn new_static(data: T) -> DataFraming<T> {
        DataFraming {
            meta: Metadata::new::<T>(),
            delete: |_| panic!("static data should never be deleted"),
            users: AtomicUsize::new(1),
            gdb_hint: Self::const_gdb_hint(),
            data,
        }
    }

    pub fn unknown_box(data: Box<Self>) -> *mut DataFraming<UnknownData> {
        Box::into_raw(data).cast()
    }

    /// SAFETY: the user must ensure that `this` is actually a an instance of Box<T>
    pub unsafe fn known_box(this: *mut DataFraming<UnknownData>) -> Box<Self> {
        Box::from_raw(this.cast())
    }
}

impl DataFraming<UnknownData> {
    pub unsafe fn to_known_ref<T: Data>(&self) -> &DataFraming<T> {
        &*(self as *const Self as *const DataFraming<T>)
    }
}

pub struct Indirection {
    data: AtomicPtr<DataFraming<UnknownData>>,
    current_gen: AtomicU64,
    //reference_count: AtomicU64,
    #[cfg(feature = "labels")]
    label: Atomic<String>,
}

pub const fn static_indirection<T: Data>(data: &'static DataFraming<T>) -> Indirection {
    Indirection {
        data: AtomicPtr::new(data as *const DataFraming<T> as *mut DataFraming<UnknownData>),
        current_gen: AtomicU64::new(0),
        #[cfg(feature = "labels")]
        label: Atomic::null(),
    }
}

pub const fn static_objectptr(indirection: &'static Indirection) -> DataPtr {
    DataPtr {
        raw: indirection,
        expected_gen: 0,
    }
}

unsafe impl Send for Indirection {}
unsafe impl Sync for Indirection {}

pub struct RecycledDataPtr(&'static Indirection);

static NULL_INDIRECTION: Indirection = Indirection {
    data: AtomicPtr::new(ptr::null_mut()),
    current_gen: AtomicU64::new(u64::MAX),
    #[cfg(feature = "labels")]
    label: Atomic::null(),
};

#[deprecated]
pub static NULL: DataPtr = DataPtr {
    raw: &NULL_INDIRECTION,
    expected_gen: 0,
};

#[derive(Clone, Copy)]
#[repr(C)]
pub struct DataPtr {
    raw: &'static Indirection,
    expected_gen: u64,
}

impl DataPtr {
    pub(crate) fn indirection_ptr(&self) -> *const Indirection {
        self.raw
    }

    pub(crate) fn expected_gen(&self) -> u64 {
        self.expected_gen
    }

    #[deprecated]
    pub fn null() -> Self {
        NULL
    }

    /// Wraps this pointer so that it is managed by the garbage collector.
    ///
    /// SAFETY: It must be sound to call the given delete impl exactly once on the given pointer,
    /// and the kill/revive impls multiple times.
    /// SAFTEY: The given pointer must be safe to dereference until the delete impl is called.
    pub unsafe fn new_raw<U: Universe>(data: *mut DataFraming<UnknownData>, _universe: U) -> Self {
        let raw = match U::recycler().recover() {
            Some(RecycledDataPtr(recycle)) => {
                recycle.data.store(data, Ordering::Relaxed);
                #[cfg(feature = "labels")]
                recycle.label.store(Shared::null(), Ordering::SeqCst);
                recycle
            }
            None => Box::leak(Box::new(Indirection {
                data: AtomicPtr::new(data),
                current_gen: AtomicU64::new(0),
                #[cfg(feature = "labels")]
                label: Atomic::null(),
            })),
        };
        let expected_gen = raw.current_gen.load(Ordering::Relaxed);
        DataPtr { raw, expected_gen }
    }

    pub fn kill<U: Universe>(
        &self,
        ctx: &mut LifecycleContext<U>,
        universe: U,
    ) -> Option<KilledData> {
        // Increment the generation counter with Release ordering so that no [WeakGrc::load] can
        // access the indirection from this moment onward. If a load has already occured and the
        // pointer is running around somewhere, the cleanup will be defered until that thread is
        // unpinned. Otherwise it may occur immediately.
        let released = self
            .raw
            .current_gen
            .compare_exchange(
                self.expected_gen,
                self.expected_gen + 1,
                Ordering::AcqRel,
                Ordering::Relaxed,
            )
            .is_ok();

        if released {
            // SAFETY: we successfuly incremented the generation counter.
            Some(unsafe { KilledData::new(*self, universe) })
        } else {
            None
        }
    }

    pub(crate) unsafe fn swap(
        &self,
        ptr: *mut DataFraming<UnknownData>,
    ) -> *mut DataFraming<UnknownData> {
        self.raw.data.swap(ptr as _, Ordering::Relaxed) as _
    }

    pub fn load_frame<'g>(&self, _guard: &'g Guard) -> Option<&'g DataFraming<UnknownData>> {
        // As long as the generation number matches, the indirection will contain the same data as
        // when self was created. The guard will prevent any deletion screwing up the reference
        // we return. If a killed pointer was revived, we need to see any changes made to that pointer
        // before reviving so we use Acquire ordering.
        let current_gen = self.raw.current_gen.load(Ordering::Acquire);
        if current_gen == self.expected_gen {
            let ptr = self.raw.data.load(Ordering::Relaxed);
            unsafe { ptr.as_ref() }
        } else {
            // No longer the same object
            None
        }
    }

    pub fn load<'g>(&self, guard: &'g Guard) -> Option<&'g UnknownData> {
        Some(&self.load_frame(guard)?.data)
    }

    pub unsafe fn load_frame_unchecked<'g>(
        &self,
        _guard: &'g Guard,
    ) -> &'g DataFraming<UnknownData> {
        &*self.raw.data.load(Ordering::Relaxed)
    }

    pub(crate) fn debug_fields(&self) -> impl fmt::Debug + '_ {
        struct DebugPtr(*const Indirection, u64, u64);
        impl fmt::Debug for DebugPtr {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                if self.1 == self.2 {
                    write!(f, "{:?}:{}", self.0, self.1)
                } else {
                    write!(f, "INVALID")
                }
            }
        }

        DebugPtr(
            self.indirection_ptr(),
            self.expected_gen,
            self.raw.current_gen.load(Ordering::Relaxed),
        )
    }

    pub fn is_null(&self) -> bool {
        let current_gen = self.raw.current_gen.load(Ordering::Relaxed);
        current_gen != self.expected_gen
    }

    #[cfg(feature = "labels")]
    pub fn store_label(self, label: impl fmt::Display) {
        let current_gen = self.raw.current_gen.load(Ordering::Acquire);
        if current_gen == self.expected_gen {
            self.raw
                .label
                .store(Owned::new(label.to_string()), Ordering::SeqCst);
        }
    }

    #[cfg(not(feature = "labels"))]
    pub fn store_label(self, _label: impl fmt::Display) {}

    #[cfg(feature = "labels")]
    pub fn load_label<'g>(self, guard: &'g Guard) -> Option<&'g str> {
        let current_gen = self.raw.current_gen.load(Ordering::Acquire);
        if current_gen == self.expected_gen {
            let l = self.raw.label.load(Ordering::SeqCst, guard);
            // SAFETY: always using seqcst
            let l = unsafe { l.as_ref()? };
            Some(l.as_str())
        } else {
            None
        }
    }

    #[cfg(not(feature = "labels"))]
    pub fn load_label<'g>(self, _guard: &'g Guard) -> Option<&'g str> {
        None
    }
}

/// Use this only for debugging purposes!
#[no_mangle]
pub(crate) unsafe extern "C" fn hornpipedbg_deref(data: DataPtr) -> *const UnknownData {
    let frame = &*data.raw.data.load(Ordering::Relaxed);
    &frame.data
}

#[test]
fn sizeof_dataptr() {
    use std::mem::size_of;
    if size_of::<*mut ()>() == 8 {
        assert_eq!(
            std::mem::size_of::<DataPtr>(),
            8 // pointer
            + 8 // version
        );
        assert_eq!(
            std::mem::size_of::<Option<DataPtr>>(),
            8 // pointer, null=None
            + 8 // version
        );
    }
}
impl cmp::PartialEq for DataPtr {
    fn eq(&self, other: &Self) -> bool {
        self.indirection_ptr() == other.indirection_ptr()
            && (self.expected_gen == other.expected_gen)
    }
}

impl cmp::Eq for DataPtr {}

impl cmp::PartialOrd for DataPtr {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl cmp::Ord for DataPtr {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        self.indirection_ptr()
            .cmp(&other.indirection_ptr())
            .then(self.expected_gen.cmp(&other.expected_gen))
    }
}

impl hash::Hash for DataPtr {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        hash::Hash::hash(&self.indirection_ptr(), state);
        hash::Hash::hash(&self.expected_gen, state);
    }
}

impl fmt::Debug for DataPtr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut f = f.debug_tuple("ObjectRef");
        let guard = pin();
        if let Some(label) = self.load_label(&guard) {
            f.field(&label);
        } else {
            f.field(&self.debug_fields());
        }
        f.finish()
    }
}

#[test]
fn sizeof_objectptr() {
    use std::mem::size_of;
    if size_of::<*mut ()>() == 8 {
        assert_eq!(
            std::mem::size_of::<DataPtr>(),
            8 // pointer
            + 8 // version
        );
        assert_eq!(
            std::mem::size_of::<Option<DataPtr>>(),
            8 // pointer, null=None
            + 8 // version
        );
    }
}

pub struct KilledData {
    // TODO: the drop impl will increment current_gen an extra time
    inner: Option<DataPtr>,
    recycler: &'static Recycler,
}

impl fmt::Debug for KilledData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut d = f.debug_tuple("KilledObject");
        d.field(self.inner.as_ref().unwrap());
        d.finish()
    }
}

impl KilledData {
    /// SAFETY: the generation counter must be incremented before construction
    unsafe fn new<U: Universe>(inner: DataPtr, _universe: U) -> Self {
        Self {
            inner: Some(inner),
            recycler: U::recycler(),
        }
    }

    pub fn revive(mut self) -> DataPtr {
        let inner = self.inner.take().unwrap();
        // Changing current_gen to a previous value is usually very dangerous. But in this case we
        // know it was incremented once by [StrongGrc::kill] _without_ invalidating any pointers
        // or recycling the indirection. By using Release ordering together with Acquire ordering in
        // ObjectRef::release, we guarentee that
        inner.raw.current_gen.fetch_sub(1, Ordering::AcqRel);
        inner
    }

    pub fn ptr(&self) -> DataPtr {
        self.inner.expect("already deleted")
    }

    pub fn get_ref(&self) -> &UnknownData {
        let allocation = self
            .inner
            .expect("already deleted")
            .raw
            .data
            .load(Ordering::Relaxed);
        // SAFTEY: only one instance of Self may exist for this pointer, so no other instance can
        // have deleted it yet. We don't even need a guard because the later delete call requires
        // mutable access to self.
        unsafe { &(*allocation).data }
    }

    pub fn delete(&mut self, guard: &Guard) {
        if let Some(inner) = self.inner.take() {
            let recycler = self.recycler;
            // The generation has already been incremented, so this is the previous generation.
            let previous_gen = inner.expected_gen;
            let call_delete = move || {
                let ptr = inner.raw.data.swap(ptr::null_mut(), Ordering::Relaxed);
                // SAFETY: The guard.defer below guarentes that we now have exclusive access.
                // SAFETY: The API requires the Delete impl to be sound.
                // SAFETY: The type can not have changed without calling `delete` previously.
                unsafe { ((*ptr).delete)(ptr) };
            };
            if previous_gen == u64::MAX - 1 {
                guard.defer(move || {
                    // Delete the data.
                    call_delete();
                    // We can not invalidate the indirection if we use it again. Free the memory
                    // but leak the indirection forever! Ideally this never happens but better
                    // safe than triggering crazy UB.
                });
            } else {
                guard.defer(move || {
                    // Delete the data.
                    call_delete();
                    // And recycle the indirection!
                    recycler.submit(RecycledDataPtr(inner.raw));
                });
            }
        }
    }
}

impl Drop for KilledData {
    fn drop(&mut self) {
        self.delete(&pin());
    }
}
