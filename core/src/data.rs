// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Responsible for the common trait shared by all hornpipe data.

use crate as hornpipe_core;
use crate::framing::UnknownData;
use crate::share::{CopyRef, OwnershipStatus, PartiallyOwned};
use crate::{framing::DataPtr, transaction::LifecycleContext};
use crate::{DefaultUniverse, TxRef, Universe};
use std::hint::black_box;
use std::mem::{offset_of, transmute};
use std::ops::{Add, Neg};
use std::{
    ffi::c_void,
    ops::{Deref, DerefMut},
};

/// The trait implemented by all hornpipe-manageable types.
///
/// The hornpipe API gives the implementer a few choices for how to handle data:
/// 1. Allow only immutable access (see `Opaque<T>` or `Own<T>`).
/// 2. Allow overwrites, leaking old values in every case (please do not this).
/// 3. Allow overwrites, with some consistent scheme for determining ownership
///    so that old values can be dropped at the right moment (see `Register<Own<T>>`).
///
/// Note that `Self` can be accessed in both managed and unmanaged contexts.
/// For example, given a user type like:
/// ```ignore
/// #[derive(Data)]
/// struct MyData {
///    field: Register<Register<i32>>,
/// }
/// ```
/// `MyData` and the outer `Register` are managed, while the inner `Register` and `i32` are
/// unmanaged. Some managed instance of `T` can be accessed from the public API as `Ref<T>`,
/// while an unmanaged instance can only ever be accessed as `&'tx T`. That is, managed
/// instances are managed by the transaction system while unmanaged instances are treated as
/// plain-old ordinary Rust types (although the parent type may still depend on some properties
/// of Data such as copy safety). Some instances will spend their whole lifecycle as managed
/// (e.g. a transaction writes some data to a register and then gets canceled), other
/// instances might be managed for most of their lifecycle but have the normal Drop impl called
/// once no outstanding `Ref`s can be used (e.g. the contents of an `Own<T>`), and still others
/// might never be managed (e.g. the contents of an `Opaque<T>`).
pub trait Data: 'static + Sync + Send + CopyRef {
    /// Generally this can default to `DefaultUniverse`.
    type Universe: Universe;

    /// Apply to `outgoing` any operations made on the transaction's `internal` state,
    /// and update `internal` so it will undo those operations later if resubmitted.
    /// The main purpose of this function is to rebase the transaction's operations
    /// onto `outgoing`, which is initialized with the latest global state. The
    /// `internal` state must also take ownership of any deleted values in order to
    /// either revive or garbage collect them at a future time, otherwise they will be
    /// permanently leaked. This function should be as fast as possible since it
    /// blocks transaction commit.
    fn finalize<'temp, 'global: 'temp>(
        internal: &'temp mut PartiallyOwned<'temp, Self>,
        outgoing: &'temp mut PartiallyOwned<'global, Self>,
        ctx: LifecycleContext<Self::Universe>,
    );
}

// The impl for plain-old-data.
impl<T: Copy + Send + Sync + 'static> Data for T {
    type Universe = DefaultUniverse;

    fn finalize<'temp, 'global: 'temp>(
        _internal: &'temp mut PartiallyOwned<'temp, T>,
        _outgoing: &'temp mut PartiallyOwned<'global, T>,
        _ctx: LifecycleContext<DefaultUniverse>,
    ) {
    }
}

pub struct Opaque<T: ?Sized, U: Universe = DefaultUniverse> {
    pub inner: Box<T>,
    _uni: U,
}

impl<T: ?Sized> Opaque<T> {
    pub fn new(inner: T) -> Self
    where
        T: Sized,
    {
        Self::new_boxed(Box::new(inner))
    }

    pub fn new_boxed(inner: Box<T>) -> Self {
        Self {
            inner,
            _uni: DefaultUniverse::THIS,
        }
    }
}

impl<T: ?Sized, U: Universe> Opaque<T, U> {
    pub fn new_in(inner: T, uni: U) -> Self
    where
        T: Sized,
    {
        Self::new_boxed_in(Box::new(inner), uni)
    }

    pub fn new_boxed_in(inner: Box<T>, uni: U) -> Self {
        Self { inner, _uni: uni }
    }
}
impl<T: ?Sized, U: Universe> Deref for Opaque<T, U> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &T {
        &*self.inner
    }
}

impl<T: ?Sized, U: Universe> DerefMut for Opaque<T, U> {
    fn deref_mut(&mut self) -> &mut T {
        &mut *self.inner
    }
}

impl<T: ?Sized + Send + Sync + 'static, U: Universe> Data for Opaque<T, U> {
    type Universe = U;

    fn finalize<'temp, 'global: 'temp>(
        _internal: &'temp mut PartiallyOwned<'temp, Self>,
        _outgoing: &'temp mut PartiallyOwned<'global, Self>,
        _ctx: LifecycleContext<Self::Universe>,
    ) {
    }
}

// SAFETY: the indirection prevents mutable access
unsafe impl<T: ?Sized, U: Universe> CopyRef for Opaque<T, U> {
    fn visit_owned(_this: &Self, _callback: &mut (impl FnMut(DataPtr) + ?Sized)) {}

    unsafe fn set_partial_ownership(
        _this: &mut PartiallyOwned<Self>,
        _matching: u8,
        _status: OwnershipStatus,
    ) {
    }

    unsafe fn partial_drop(_this: &mut PartiallyOwned<Self>) {}
}

/// Any copy type can be treated as data! There is also a blanket impl `T: Data` you
/// should be using instead if you are in the default universe.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug, Default)]
pub struct Pod<T, U: Universe = DefaultUniverse> {
    pub data: T,
    _uni: U,
}

impl<T, U: Universe> Pod<T, U> {
    pub const fn new(data: T) -> Self {
        Pod {
            data,
            _uni: U::THIS,
        }
    }
}

impl<T, U: Universe> Deref for Pod<T, U> {
    type Target = T;

    #[inline(always)]
    fn deref(&self) -> &T {
        &self.data
    }
}

impl<T, U: Universe> DerefMut for Pod<T, U> {
    fn deref_mut(&mut self) -> &mut T {
        &mut self.data
    }
}

impl<T, U: Universe> Add<Pod<T, U>> for Pod<T, U>
where
    T: Add<T>,
{
    type Output = Pod<T::Output, U>;

    fn add(self, rhs: Self) -> Pod<T::Output, U> {
        Pod {
            data: self.data + rhs.data,
            _uni: U::THIS,
        }
    }
}

impl<T, U: Universe> Neg for Pod<T, U>
where
    T: Neg,
{
    type Output = Pod<T::Output, U>;

    fn neg(self) -> Pod<T::Output, U> {
        Pod {
            data: -self.data,
            _uni: U::THIS,
        }
    }
}
// SAFETY: `T` is `Copy`, this is the only easy case!
unsafe impl<T: Copy, U: Universe> CopyRef for Pod<T, U> {
    fn visit_owned(_this: &Self, _callback: &mut (impl FnMut(DataPtr) + ?Sized)) {}

    unsafe fn set_partial_ownership(
        _this: &mut PartiallyOwned<Self>,
        _matching: u8,
        _status: OwnershipStatus,
    ) {
    }

    unsafe fn partial_drop(_this: &mut PartiallyOwned<Self>) {}
}

impl<T: Copy + Send + Sync + 'static, U: Universe> Data for Pod<T, U> {
    type Universe = U;

    fn finalize<'temp, 'global: 'temp>(
        _internal: &'temp mut PartiallyOwned<'temp, Self>,
        _outgoing: &'temp mut PartiallyOwned<'global, Self>,
        _ctx: LifecycleContext<Self::Universe>,
    ) {
    }
}

/// Exactly like [Option], except it implements `Data` even when `T: !Copy`,
/// and provides for `TxRef` projection.
#[derive(CopyRef, Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Maybe<T> {
    Just(T),
    Nothing,
}

impl<T> Default for Maybe<T> {
    fn default() -> Self {
        Maybe::Nothing
    }
}

impl<T> From<Maybe<T>> for Option<T> {
    fn from(value: Maybe<T>) -> Self {
        use Maybe::*;
        match value {
            Just(x) => Some(x),
            Nothing => None,
        }
    }
}

impl<T> From<Option<T>> for Maybe<T> {
    fn from(value: Option<T>) -> Self {
        use Maybe::*;
        match value {
            Some(x) => Just(x),
            None => Nothing,
        }
    }
}

impl<'a, T: CopyRef> PartiallyOwned<'a, Maybe<T>> {
    pub fn as_maybe_shared(&mut self) -> Maybe<&mut PartiallyOwned<'a, T>> {
        use Maybe::*;
        // SAFETY: no drop is made through the mut
        match **self {
            Just(_) => Just(unsafe { self.offset_mut(offset_of!(Maybe<T>, Just.0)) }),
            Nothing => Nothing,
        }
    }
}

impl<'tx, T: Data> TxRef<'tx, Maybe<T>> {
    pub fn as_ref(self) -> Option<TxRef<'tx, T>> {
        let offset = offset_of!(Maybe<T>, Just.0)
            .try_into()
            .expect("offset is unusually large");
        match *self {
            // SAFETY: offset is correctly calculated
            Maybe::Just(_) => Some(unsafe { TxRef::offset(self, offset) }),
            Maybe::Nothing => None,
        }
    }
}

impl<T: Data> Data for Maybe<T> {
    type Universe = T::Universe;

    fn finalize<'temp, 'global: 'temp>(
        internal: &'temp mut PartiallyOwned<'temp, Maybe<T>>,
        outgoing: &'temp mut PartiallyOwned<'global, Maybe<T>>,
        ctx: LifecycleContext<<Maybe<T> as Data>::Universe>,
    ) {
        match (internal.as_maybe_shared(), outgoing.as_maybe_shared()) {
            (Maybe::Just(internal), Maybe::Just(outgoing)) => T::finalize(internal, outgoing, ctx),
            (Maybe::Nothing, Maybe::Nothing) => (),
            _ => panic!("mismatch in maybe"),
        }
    }
}

/// See the trait version of this table: [Operations].
#[derive(Copy, Clone, Debug)]
#[repr(C)]
#[allow(improper_ctypes_definitions)]
pub struct Operations {
    pub set_partial_ownership:
        unsafe extern "C" fn(*mut c_void, u8, crate::share::OwnershipStatus, *const c_void),
    pub visit_owned: unsafe extern "C" fn(*const c_void, &mut dyn FnMut(DataPtr), *const c_void),
    pub finalize: unsafe extern "C" fn(
        *mut c_void,
        *mut c_void,
        LifecycleContext<DefaultUniverse>,
        *const c_void,
    ),
    pub partial_drop: unsafe extern "C" fn(*mut c_void, *const c_void),
    pub payload: *const c_void,
}

impl Operations {
    pub unsafe fn set_partial_ownership(
        &self,
        this: &mut PartiallyOwned<UnknownData>,
        matching: u8,
        status: crate::share::OwnershipStatus,
    ) {
        (self.set_partial_ownership)(transmute(this), matching, status, self.payload)
    }

    pub unsafe fn visit_owned(&self, this: &UnknownData, callback: &mut dyn FnMut(DataPtr)) {
        (self.visit_owned)(transmute(this), transmute(callback), self.payload)
    }

    pub unsafe fn finalize<'a, U: Universe>(
        &self,
        internal: &mut PartiallyOwned<'a, UnknownData>,
        outgoing: &mut PartiallyOwned<'a, UnknownData>,
        ctx: LifecycleContext<U>,
    ) {
        (self.finalize)(
            transmute(internal),
            transmute(outgoing),
            transmute(ctx),
            self.payload,
        )
    }

    pub unsafe fn partial_drop<'a, U: Universe>(
        &self,
        internal: &mut PartiallyOwned<'a, UnknownData>,
    ) {
        (self.partial_drop)(transmute(internal), self.payload);
    }
}

unsafe impl Sync for Operations {}
unsafe impl Send for Operations {}

pub const fn data_operations<T: Data>() -> Operations {
    unsafe extern "C" fn set_partial_ownership<T: Data>(
        this: *mut c_void,
        matching: u8,
        status: OwnershipStatus,
        _payload: *const c_void,
    ) {
        <T as CopyRef>::set_partial_ownership(transmute(this), matching, status)
    }

    unsafe extern "C" fn visit_owned<T: Data>(
        this: *const c_void,
        callback: &mut dyn FnMut(DataPtr),
        _payload: *const c_void,
    ) {
        T::visit_owned(transmute(this), callback);
    }

    unsafe extern "C" fn finalize<'a, T: Data>(
        internal: *mut c_void,
        outgoing: *mut c_void,
        ctx: LifecycleContext<DefaultUniverse>,
        _payload: *const c_void,
    ) {
        T::finalize(transmute(internal), transmute(outgoing), transmute(ctx))
    }

    unsafe extern "C" fn partial_drop<'a, T: CopyRef>(
        internal: *mut c_void,
        _payload: *const c_void,
    ) {
        <T as CopyRef>::partial_drop(transmute(internal));
    }

    let ops = Operations {
        set_partial_ownership: set_partial_ownership::<T>,
        visit_owned: visit_owned::<T>,
        finalize: finalize::<T>,
        partial_drop: partial_drop::<T>,
        payload: std::ptr::null_mut(),
    };
    black_box(&ops);
    ops
}
