//! Responsible for all singletons.

use crate as hornpipe_core;
use crate::framing::RecycledDataPtr;
use crate::transaction::Sequencer;
use crate::utex::UnihandleLock;
use crate::{Data, Ref};
use crossbeam_queue::SegQueue;
use hornpipe_macros::CopyRef;
use std::marker::PhantomData;

#[derive(CopyRef)]
pub struct UniverseInfo<U: Universe> {
    #[unsafe_skip]
    pub uni: PhantomData<U>,
}

impl<U: Universe> Data for UniverseInfo<U> {
    type Universe = U;

    fn finalize<'temp, 'global: 'temp>(
        _internal: &'temp mut crate::share::PartiallyOwned<'temp, Self>,
        _outgoing: &'temp mut crate::share::PartiallyOwned<'global, Self>,
        _ctx: crate::transaction::LifecycleContext<Self::Universe>,
    ) {
    }
}

pub struct Recycler(SegQueue<RecycledDataPtr>);

impl Recycler {
    pub const fn new() -> Self {
        Recycler(SegQueue::new())
    }

    pub fn submit(&self, raw: RecycledDataPtr) {
        self.0.push(raw);
    }

    pub fn recover(&self) -> Option<RecycledDataPtr> {
        self.0.pop()
    }
}

/// All the global variables in hornpipe.
pub trait Universe:
    // Implements all these traits just to fix derive macros on universe-parameterized types.
    Copy + Clone + Default + std::fmt::Debug + PartialEq + Eq + PartialOrd + Ord + std::hash::Hash + Sync + Send + Sized + 'static
{
    const THIS: Self;

    fn recycler() -> &'static Recycler;
    fn sequencer() -> &'static Sequencer<Self>;
    fn handle() -> &'static UnihandleLock<Self>;
    fn info() -> Ref<UniverseInfo<Self>>;
}

#[macro_export]
macro_rules! declare_universe {
    ($(#[$m:meta])* $vis:vis $name:ident) => {
        $(#[$m])*
        #[derive(Copy, Clone, PartialEq, Eq, Debug, PartialOrd, Ord, Hash)]
        $vis struct $name;

        impl Default for $name {
            fn default() -> Self {
                $name
            }
        }

        impl $crate::Universe for $name {
            const THIS: Self = $name;

            fn recycler() -> &'static $crate::universe::Recycler {
                static R: $crate::universe::Recycler = $crate::universe::Recycler::new();
                &R
            }

            fn sequencer() -> &'static $crate::transaction::Sequencer<Self> {
                static S: $crate::transaction::Sequencer<$name> = $crate::transaction::Sequencer::new();
                &S
            }

            fn handle() -> &'static $crate::utex::UnihandleLock<Self> {
                static S: $crate::utex::UnihandleLock<$name> = $crate::utex::UnihandleLock::new({
                    // SAFETY: only one impl Universe can exist for $name, and we are that impl
                    unsafe { $crate::utex::new_universe_handle() }
                });
                &S
            }

            fn info() -> $crate::Ref<$crate::universe::UniverseInfo<Self>> {
                $crate::static_ref! {
                    static INFO: Ref<$crate::universe::UniverseInfo<$name>> = $crate::universe::UniverseInfo {
                        uni: std::marker::PhantomData,
                    };
                }
                INFO
            }
        }
    }
}

declare_universe! {
    /// All the default global variables in hornpipe.
    pub DefaultUniverse
}
