// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use self::stackmap::StackMap;
use crate as hornpipe_core;
use crate::framing::DataPtr;
use crate::refs::{OwnOperations, PtrCmp};
use crate::share::{CopyRef, OwnershipFlagged, PartiallyOwned};
use crate::transaction::LifecycleContext;
use crate::{Data, Operations, Ref};
use crossbeam_epoch::Guard;
use im::HashMap;
use std::borrow::{Borrow, Cow};
use std::cmp;
use std::mem::replace;
use std::sync::atomic::AtomicPtr;
use std::sync::atomic::Ordering::Relaxed;

mod stackmap;

pub trait Keyed: CopyRef + Send + Sync + 'static {
    type Key: Ord + Clone + Send + Sync + 'static;

    fn key(&self, guard: &Guard) -> Cow<Self::Key>;
}

impl<T> Keyed for T
where
    T: Ord + Copy + Send + Sync + 'static,
{
    type Key = T;

    fn key(&self, _guard: &Guard) -> Cow<T> {
        Cow::Borrowed(self)
    }
}

impl<T> Keyed for Ref<T>
where
    T: Send + Sync + 'static,
{
    type Key = PtrCmp<Ref<T>>;

    fn key(&self, _guard: &Guard) -> Cow<Self::Key> {
        Cow::Owned(PtrCmp(*self))
    }
}

impl<T: Keyed> Keyed for OwnershipFlagged<T> {
    type Key = T::Key;

    fn key(&self, guard: &Guard) -> Cow<T::Key> {
        T::key(self, guard)
    }
}

const FANOUT: usize = 64;

struct NodePointer<T: Keyed> {
    ptr: AtomicPtr<Node<T>>,
    lo: T::Key,
}

impl<T: Keyed> NodePointer<T> {
    pub fn new(node: Node<T>, guard: &Guard) -> Self {
        let lo = match &node {
            Node::Leaf(map) => map.first().unwrap().key(guard).into_owned(),
            Node::Branch(map) => map.first().unwrap().lo.clone(),
        };
        Self::new_with_lo(node, lo)
    }

    pub fn new_with_lo(node: Node<T>, lo: T::Key) -> Self {
        let ptr = AtomicPtr::new(Box::into_raw(Box::new(node)));
        Self { lo, ptr }
    }

    pub fn node_ref(&self, _guard: &Guard) -> &Node<T> {
        unsafe { &*self.ptr.load(Relaxed) }
    }

    pub fn node_mut(&mut self, _guard: &Guard) -> &mut Node<T> {
        unsafe { &mut *self.ptr.load(Relaxed) }
    }
}

impl<T: Keyed> Drop for NodePointer<T> {
    fn drop(&mut self) {
        drop(unsafe { Box::from_raw(*self.ptr.get_mut()) });
    }
}

unsafe impl<T: Keyed> CopyRef for NodePointer<T> {
    fn visit_owned(this: &Self, callback: &mut (impl FnMut(DataPtr) + ?Sized)) {
        todo!()
    }

    unsafe fn set_partial_ownership(
        this: &mut PartiallyOwned<Self>,
        matching: u8,
        status: crate::share::OwnershipStatus,
    ) {
        todo!()
    }

    unsafe fn partial_drop(this: &mut PartiallyOwned<Self>) {
        todo!()
    }
}

impl<T: Keyed> Keyed for NodePointer<T> {
    type Key = T::Key;

    fn key(&self, guard: &Guard) -> Cow<T::Key> {
        Cow::Borrowed(&self.lo)
    }
}

type LeafMap<T> = StackMap<T, FANOUT>;
type BranchMap<T> = StackMap<OwnershipFlagged<NodePointer<T>>, FANOUT>;

#[derive(CopyRef)]
enum Node<T: Keyed> {
    Leaf(LeafMap<T>),
    Branch(BranchMap<T>),
}

impl<T: Keyed> Node<T> {
    pub fn first_key(&self, guard: &Guard) -> Option<Cow<T::Key>> {
        match self {
            Node::Leaf(map) => Some(map.first()?.key(guard)),
            Node::Branch(map) => Some(Cow::Borrowed(&map.first()?.lo)),
        }
    }

    pub fn insert(&mut self, item: T, guard: &Guard) -> (Option<T>, Option<NodePointer<T>>) {
        match self {
            Node::Leaf(map) => {
                if map.len() == FANOUT {
                    let mut other = map.split_off(map.len() / 2);
                    let lo = other.first().unwrap().key(guard).into_owned();
                    let prev = if item.key(guard) < Cow::Borrowed(&lo) {
                        map.insert(item, guard)
                    } else {
                        other.insert(item, guard)
                    };
                    (prev, Some(NodePointer::new_with_lo(Node::Leaf(other), lo)))
                } else {
                    (map.insert(item, guard), None)
                }
            }
            Node::Branch(map) => {
                // the lo value of the first element is allowed to be higher
                let Some(ptr) = map.get_less_than_or_equal_or_first(&item.key(guard), guard) else {
                    return (None, None);
                };
                let node = ptr.node_mut(guard);
                let (prev, add) = node.insert(item, guard);

                if let Some(add) = add {
                    let add = OwnershipFlagged::new_owned(add);
                    if map.len() == FANOUT {
                        let mut other = map.split_off(map.len() / 2);
                        let lo = other.first().unwrap().key(guard).into_owned();
                        if add.lo < lo {
                            assert!(map.insert(add, guard).is_none());
                        } else {
                            assert!(other.insert(add, guard).is_none());
                        }
                        (
                            prev,
                            Some(NodePointer::new_with_lo(Node::Branch(other), lo)),
                        )
                    } else {
                        assert!(map.insert(add, guard).is_none());
                        (prev, None)
                    }
                } else {
                    (prev, None)
                }
            }
        }
    }
}

pub struct Tree<T: Keyed> {
    root: Node<T>,
}

impl<T: Keyed> Tree<T> {
    pub fn new() -> Self {
        Tree {
            root: Node::Leaf(StackMap::new()),
        }
    }

    pub fn get(&self, key: &T::Key, guard: &Guard) -> Option<&T> {
        let mut node = &self.root;
        loop {
            match node {
                Node::Leaf(leaf) => return leaf.get(key, guard),
                Node::Branch(children) => {
                    // the lo value of the first element is allowed to be higher
                    let ptr = children.get_less_than_or_equal_or_first(key, guard)?;
                    node = ptr.node_ref(guard);
                }
            }
        }
    }

    pub fn insert(&mut self, item: T, guard: &Guard) -> Option<T> {
        let (prev, add) = self.root.insert(item, guard);
        if let Some(add) = add {
            let old_root = replace(&mut self.root, Node::Branch(StackMap::new()));
            let Node::Branch(new_root) = &mut self.root else {
                unreachable!()
            };
            new_root.insert(
                OwnershipFlagged::new_owned(NodePointer::new(old_root, guard)),
                guard,
            );
            new_root.insert(OwnershipFlagged::new_owned(add), guard);
        }
        prev
    }
}

#[test]
fn test_tree_fill() {
    use crossbeam_epoch::pin;
    use std::collections::HashSet;

    let range_end = 64 * 64 * 2 + 10;
    let mut values: HashSet<_> = (0..range_end).collect();
    let mut tree = Tree::new();
    let guard = pin();
    for x in values.iter().copied() {
        tree.insert(x, &guard);
    }

    for x in 0..range_end {
        assert_eq!(tree.get(&x, &guard), Some(&x));
    }
    for x in -10..0 {
        assert_eq!(tree.get(&x, &guard), None);
    }
    for x in range_end..(range_end + 10) {
        assert_eq!(tree.get(&x, &guard), None);
    }
}
