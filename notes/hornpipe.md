Hornpipe is a *state management framework*. In this context state means:
- arbitrary data
- which can be mutated by any subsystem
- which is shared across all subsystems

Hornpipe's public API tries to provide these properties as directly as possible. For example:
- the `#[state]` annotation
- the  `TxRef<Register<T>>::set` method
- the `Copy + Sync + Send + 'static` impls on `Ref<T>`

Hornpipe's internals are then exist to deal with the consequences. Again:
- Weird [[object layout]] shenanigans
- Some stupid [[CRDTs]] 
- [[atomic datastructures]], [[weak refs]], [[garbage collecting]]

Additionally, there are user-facing consequences to arbitrary shared mutation which hornpipe helps to mitigate with a few key features (in no particular order):
- [[automatic dataflow]]
- [[weak refs]]
- [[transactions]]