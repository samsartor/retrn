Older versions of hornpipe had a very strict object layout, with objects basically acting as arrays of `enum Slot` instances. However, by allowing arbitrary offsets into objects they can be laid out in whatever fashion the user desires. Generally the user will want to use their own language's struct layout, but you never know.

However, there are still a few requirements:
- Objects must have a header where `Metadata` is stored, in particular the vtable of function pointers used for the [[swap hack]]
- 