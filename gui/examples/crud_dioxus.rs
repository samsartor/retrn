use dioxus::prelude::*;
use hornpipe_gui::dioxus::{use_hornpipe, BiInput};
use hornpipe_interface::{hornpipe_extern, prelude::*, Slot};
use tracing::trace;

fn main() {
    hornpipe_core::tracing_on();
    dioxus_desktop::launch(app);
}

hornpipe_extern! {
    struct Person {
        mut name: Own<Text>,
        mut surname: Own<Text>,
        computed full_name: Own<Text>,
    }

    struct App {
        mut selected: Ref<Person>,
        mut filter: Own<Text>,
        mut people: Own<OwnSet<Person>>,
        computed visible_people: Own<RefSet<Person>>,
    }
}

impl Person {
    fn full_name(this: RefTx<'_, Self>) -> Own<Text> {
        Text::new(&format!("{} {}", this.name().get(), this.surname().get()))
    }
}

impl App {
    fn visible_people(this: RefTx<'_, Self>) -> Own<RefSet<Person>> {
        let filter = this.filter();
        this.people().get().filter_map(move |p| {
            let filter = filter.detached.attach(p.tx)?.get();
            if !filter.is_empty() && !p.full_name().get().contains(filter.as_str()) {
                return None;
            }
            Some(p.detached)
        })
    }
}

#[inline_props]
#[allow(non_snake_case)]
fn PersonView(cx: Scope, person: Slot<Ref<Person>>) -> Element {
    use_hornpipe(&cx, |tx| {
        let Some(person) = person.attach(tx).and_then(|p| p.get()) else {
            return Element::None;
        };
        let full_name = person.full_name().get();
        person.detached.label(format_args!("person {}", full_name));
        cx.render(rsx! {
                h2 {
                    "Editing {full_name}",
                }
                div {
                    span {
                        label {
                            r#for: "first_name",
                            "Name:",
                        }
                        BiInput {
                            id: "first_name",
                            value: person.name().detached,
                            parser: |t, dst| Ok(dst.set(Text::new(t))),
                            shower: |v| Text::new(&v.get()),
                        }
                    }
                    span {
                        label {
                            r#for: "last_name",
                            "Surname:",
                        }
                        BiInput {
                            id: "last_name",
                            value: person.surname().detached,
                            parser: |t, dst| Ok(dst.set(Text::new(t))),
                            shower: |v| Text::new(&v.get()),
                        }
                    }
                }
        })
    })
}

#[inline_props]
#[allow(non_snake_case)]
fn PersonSelector(cx: Scope, root: Ref<App>, person: Ref<Person>) -> Element {
    use_hornpipe(&cx, |tx| {
        let style = match root.attach(tx).and_then(|r| r.selected().get()) {
            Some(p) if &p.detached == person => "border: solid 2px blue;",
            _ => "",
        };
        let Some(p) = person.attach(tx) else {
            return Element::None;
        };
        let full_name = p.full_name().get();
        let key = p.detached.unique_string();
        cx.render(rsx! {
            li {
                key: "{key}",
                button {
                    onclick: move |_| root.operate(|root| {
                        root.selected().set(p.detached);
                    }),
                    style: "{style}",
                    "{full_name}"
                }
            }
        })
    })
}

fn app(cx: Scope) -> Element {
    use_hornpipe(&cx, |tx| {
        let root = use_state(&cx, || App::new(Ref::null(), "", OwnSet::<Person>::new())).attach(tx);
        root.detached.label("root");
        root.people().get().detached.label("people list");
        root.visible_people()
            .get()
            .detached
            .label("visible people list");

        trace!(target: "root_rerender", tx=?tx.refer());

        cx.render(rsx! {
            span {
                label {
                    r#for: "filter_str",
                    "Filter:",
                }
                BiInput {
                    id: "filter_str",
                    value: root.filter().detached,
                    parser: |t, dst| Ok(dst.set(Text::new(t))),
                    shower: |v| Text::new(&v.get().to_string()),
                }
            }
            div {
                rsx! { PersonView { person: root.selected().detached } }
            }
            h2 {
                "People"
            }
            ul {
                root
                    .visible_people()
                    .get()
                    .iter()
                    .filter_map(|p| p)
                    .map(|p| rsx! { PersonSelector {
                        root: root.detached,
                        person: p.detached,
                    } })
            }
            button {
                onclick: move |_| root.detached.operate(|root| {
                    let p = Person::new("", "");
                    p.refer().label("new person");
                    root.selected().set(p.refer());
                    root.people().get().insert(&p.refer(), p);
                }),
                "Add"
            }
        })
    })
}
