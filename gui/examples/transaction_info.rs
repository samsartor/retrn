use hornpipe_interface::prelude::*;
use hornpipe_interface::{GlobalInfo, Stream, StreamTerminal};
use once_cell::sync::Lazy;

hornpipe_extern! {
    struct App {
        mut count: i32,
    }
}

static ROOT: Lazy<Own<App>> = Lazy::new(|| App::new(0));

fn main() {
    use std::env::args;

    match args().nth(1).as_deref() {
        #[cfg(feature = "dioxus")]
        Some("dioxus") => {
            dioxus_desktop::launch(crate::dioxus::app);
        }
        #[cfg(feature = "xilem")]
        Some("xilem") => {
            let app = ::xilem::App::new((), crate::xilem::app);
            ::xilem::AppLauncher::new(app).run()
        }
        #[cfg(feature = "druid")]
        Some("druid") => {
            let app = ::druid::WindowDesc::new(crate::druid::app_widget());
            ::druid::AppLauncher::with_window(app).launch(()).unwrap();
        }
        Some(backend) => panic!("not compiled with {backend:?}"),
        None => panic!("pass a backend"),
    }
}

#[cfg(feature = "xilem")]
mod xilem {}

#[cfg(feature = "druid")]
mod druid {
    use druid::widget::{Button, Flex, Label, Widget};
    use hornpipe_gui::druid::WidgetExt as _;
    use hornpipe_interface::prelude::*;

    pub fn app_widget() -> impl Widget<()> {
        let count = super::ROOT.operate(|root| root.count().detached);
        Flex::row()
            .with_child(Label::new("").on_react(move |tx, label, ctx| {
                label.set_text(count.attach(tx).unwrap().get().to_string());
                ctx.request_update();
            }))
            .with_child(Button::new("Count Up").on_click(move |_, _, _| {
                count.operate(|c| c.set(c.get() + 1));
            }))
            .with_child(Button::new("Count Down").on_click(move |_, _, _| {
                count.operate(|c| c.set(c.get() - 1));
            }))
    }
}

#[cfg(feature = "dioxus")]
mod dioxus {
    use dioxus::prelude::*;
    use hornpipe_gui::dioxus::use_hornpipe;

    pub fn app(cx: Scope) -> Element {
        let term = use_state(|&cx| {
            use_hornpipe(&cx, |tx| {
                GlobalInfo::get()
                    .attach(tx)
                    .unwrap()
                    .submitted()
                    .attach(tx)
                    .create_terminal()
            })
        });
        use_future(&cx, , )
        cx.render(rsx! {
            label { count.get().to_string() }
            button {
                onclick: move |_| count.detached.operate(|c| c.set(c.get() + 1)),
                "Count Up",
            }
            button {
                onclick: move |_| count.detached.operate(|c| c.set(c.get() - 1)),
                "Count Down",
            }
        })
    }
}
