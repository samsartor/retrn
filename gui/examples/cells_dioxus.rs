use dioxus::prelude::*;
use hornpipe_gui::dioxus::use_hornpipe;
use hornpipe_interface::prelude::*;
use hornpipe_interface::{core::AttachedObjectRef, hornpipe_extern};
use meval::{eval_str_with_context, ContextProvider};

fn main() {
    dioxus_desktop::launch(app);
}

hornpipe_extern! {
    struct Output {
        val: f64,
        mut err: Option<Own<Text>>,
    }

    struct Cell {
        mut form: Own<Text>,
        mut table: Ref<Table>,
        computed output: Own<Output>,
    }

    struct Table {
        mut cells: Own<Collection<[u32], Own<Cell>>>,
    }

    struct App {
        mut table: Own<Table>,
    }
}

struct Ctx<'tx>(RefTx<'tx, Table>);

impl ContextProvider for Ctx<'_> {
    fn get_var(&self, name: &str) -> Option<f64> {
        let mut c = name.chars();
        let x: u32 = c.next()?.to_ascii_uppercase() as u32 - 'A' as u32;
        let y: u32 = c.as_str().parse().ok()?;
        let y: u32 = y.checked_sub(1)?.try_into().ok()?;
        Some(self.0.cells().get().find(&[x, y])?.val())
    }

    fn eval_func(&self, _: &str, _: &[f64]) -> Result<f64, meval::FuncEvalError> {
        Err(meval::FuncEvalError::UnknownFunction)
    }
}

impl Cell {
    pub fn output(this: RefTx<Self>) -> Own<Output> {
        let Some(table) = this.table().get() else {
            return Output::new(0.0, Some(Text::new("table missing")));
        };
        let (val, err) = match eval_str_with_context(&this.form().get().to_string(), Ctx(table)) {
            Ok(val) => (val, None),
            Err(err) => (
                this.output()
                    .opt()
                    .get()
                    .map(|out| out.val())
                    .unwrap_or(0.0),
                Some(Text::new(&err.to_string())),
            ),
        };
        Output::new(val, err)
    }
}

impl<'tx> Cell<AttachedObjectRef<'tx>> {
    pub fn val(&self) -> f64 {
        self.output().get().val()
    }

    pub fn val_str(&self) -> String {
        match self.output().get().err().get() {
            Some(err) => err.to_string(),
            None => self.val().to_string(),
        }
    }
}

#[derive(Props, PartialEq, Eq)]
struct CellInputProps {
    x: u32,
    y: u32,
    table: Ref<Table>,
}

#[allow(non_snake_case)]
fn CellInput<'a>(cx: Scope<'a, CellInputProps>) -> Element {
    let CellInputProps { x, y, table } = *cx.props;
    eprintln!("Rerendering {x},{y}");
    cx.render(use_hornpipe(&cx, |tx| {
        let selected = use_state(&cx, || false);
        let Some(table) = table.attach(tx) else { return rsx! { td { } } };
        if let Some(cell) = table.cells().get().find(&[x, y]) {
            let (value, hover) = if *selected.get() {
                (cell.form().get().to_string(), cell.val_str())
            } else {
                (cell.val_str(), cell.val().to_string())
            };
            rsx! {
                td {
                    style: "width: 100px; height: 25px; border: 1px solid gray;",
                    input {
                        style: "display: block; margin: 0 0; padding 0 0; width: 98px; height: 23px; border: none;",
                        r#type: "text",
                        value: "{value}",
                        autofocus: "true",
                        title: "{hover}",
                        onfocusin: |_| selected.set(true),
                        onfocusout: |_| selected.set(false),
                        oninput: move |evt| {
                            let tx = Transaction::start();
                            let Some(cell) = cell.detached.attach(&tx) else { return };
                            cell.form().set(Text::new(&evt.data.value));
                            tx.commit();
                        }
                    }
                }
            }
        } else {
            rsx! {
                td {
                    style: "width: 100px; height: 25px; border: 1px solid gray;",
                    onclick: move |_| {
                        let tx = Transaction::start();
                        let Some(table) = table.detached.attach(&tx) else { return };
                        let cell = Cell::new(Text::new(""), table.detached);
                        cell.refer().label(format_args!("cell {x},{y}"));
                        table.cells().get().insert(&[x, y], cell);
                        tx.commit();
                    }
                }
            }
        }
    }))
}

fn app(cx: Scope) -> Element {
    let root = use_state(&cx, || {
        let cells = Collection::new();
        cells.refer().label("cells");
        let table = Table::new(cells);
        table.refer().label("table");
        let app = App::new(table);
        app.refer().label("app");
        app
    });

    use_hornpipe(&cx, |tx| {
        let table = root.attach(tx).table().get();
        let last_col = 'Z';
        let width = (last_col as u32 - 'A' as u32 + 2) * 100;
        let last_row = 50;
        cx.render(rsx! {
            div {
                style: "
                margin: 0 0;
                overflow: auto;
                ",
                table {
                    style: "
                    border-collapse: collapse;
                    display: block;
                    width: {width}px;
                    white-space: nowrap;
                    ",
                    tr {
                        th { },
                        ('A'..=last_col).map(move |col| {
                            rsx! {
                                th { "{col}" }
                            }
                        })
                    },
                    (1..=last_row).enumerate().map(move |(y, row)| rsx! {
                        tr {
                            td { "{row}" }
                            ('A'..=last_col).enumerate().map(move |(x, _)| {
                                rsx! {
                                    CellInput {
                                        key: "{x},{y}",
                                        x: x as u32,
                                        y: y as u32,
                                        table: table.detached,
                                    }
                                }
                            })
                        }
                    })
                }
            }
        })
    })
}
