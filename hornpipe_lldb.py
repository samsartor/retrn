import lldb

def __lldb_init_module(debugger, internal_dict):
    debugger.HandleCommand('type synthetic add -l hornpipe_lldb.RefSyntheticProvider -x "^hornpipe_core::refs::(Ref|Own)<.+>$" --category hornpipe')
    debugger.HandleCommand('type synthetic add -l hornpipe_lldb.TxRefSyntheticProvider -x "^hornpipe_core::refs::TxRef<.+>$" --category hornpipe')
    debugger.HandleCommand('type summary add -F hornpipe_lldb.ref_summary -e -x -h "^hornpipe_core::refs::(Ref|Own|TxRef)<.+>$" --category hornpipe')
    debugger.HandleCommand('type category enable hornpipe')

def txref_summary(r: lldb.SBValue, internal_dict):
    tx: lldb.SBValue = r.GetChildMemberWithName('tx')
    ty: lldb.SBType = r.type.template_args[0]
    return f'TxRef<{ty.name}>'


def ref_summary(r: lldb.SBValue, internal_dict, rty=None):
    rty = r.type.name.split('<')[0].removeprefix('hornpipe_core::refs::')
    if rty == 'TxRef':
        r = r.GetChildMemberWithName('detach')
    s = RefSyntheticProvider(r.GetNonSyntheticValue(), internal_dict)
    label = ''
    if s.label is not None and s.label.summary is not None and s.label.summary != '""':
        label = f', label={s.label.summary}'
    offset = ''
    if rty != 'Own':
        offset_num = r.GetNonSyntheticValue().GetChildMemberWithName('off').GetValue()
        offset = f', offset={offset_num}'
    if s.ind_ptr is None:
        return f'{rty}<{s.ty.name}>(NULL)'
    elif s.found_gen == s.expected_gen:
        return f'{rty}<{s.ty.name}>({s.ind_ptr.GetValue()}:{s.expected_gen}{offset}{label})'
    else:
        return f'{rty}<{s.ty.name}>({s.ind_ptr.GetValue()}:{s.found_gen}!={s.expected_gen}{offset}{label})'

def get_expression_path(val: lldb.SBValue):
    """Compute the expression path for the given value."""

    stream = lldb.SBStream()
    if not val.GetExpressionPath(stream):
        return None
    return stream.GetData()

class RefSyntheticProvider:
    def __init__(self, valobj: lldb.SBValue, internal_dict):
        self.valobj = valobj
        self.update()

    def num_children(self) -> int:
        if self.frame is None:
            return 0
        else:
            return len(self.frame.children) + (1 if self.label is not None else 0)

    def get_child_index(self, name) -> int:
        if self.frame is None:
            return -1
        elif name == 'data':
            return self.num_children() - 1
        elif name == 'label' and self.label is not None:
            return self.num_children() - 2
        else:
            return self.frame.GetIndexOfChildWithName(name)

    def get_child_at_index(self, index) -> lldb.SBValue:
        assert self.frame is not None
        assert self.data is not None
        if index == self.num_children() - 1:
            return self.data
        elif self.label is not None and index == self.num_children() - 2:
            return self.label
        else:
            return self.frame.GetChildAtIndex(index)

    def update(self):
        self.frame = None
        self.data = None
        self.ind_ptr = None
        self.label = None
        self.expected_gen = '0'
        self.found_gen = '0'

        self.ty: lldb.SBType = self.valobj.type.template_args[0]

        target: lldb.SBTarget = self.valobj.target
        dataptr_ty: lldb.SBType = target.FindFirstType("hornpipe_core::framing::DataPtr")
        dataptr: lldb.SBValue = self.valobj.GetChildMemberWithName('raw').Cast(dataptr_ty)
        assert dataptr.IsValid()
    
        self.ind_ptr = dataptr.GetChildMemberWithName('raw')
        self.expected_gen: str = dataptr.GetChildMemberWithName('expected_gen').GetValue()

        assert self.ind_ptr is not None
        ind: lldb.SBValue = self.ind_ptr.Dereference()
        if ind.IsValid():
            label_val: lldb.SBValue = ind.GetChildMemberWithName('label')
            if label_val.IsValid():
                self.label = label_val.Cast(target.FindFirstType("String").GetPointerType())
            self.found_gen: str = ind.GetChildMemberWithName('current_gen').GetChildMemberWithName('v').GetChildMemberWithName('value').GetValue()
            if self.found_gen == self.expected_gen:
                frame_ty: lldb.SBType = target.FindFirstType("hornpipe_core::framing::DataFraming<hornpipe_core::framing::UnknownData>")
                frame_ptr: lldb.SBValue = ind.GetChildMemberWithName('data').Cast(frame_ty.GetPointerType())
                self.frame = frame_ptr.Dereference()
                assert self.frame is not None
                assert self.frame.IsValid()
                data_offset = self.frame.type.fields[-1].byte_offset
                self.data = self.frame.CreateChildAtOffset('data', data_offset, self.ty)
                assert self.data is not None
                assert self.data.IsValid()
        else:
            self.ind_ptr = None

    def has_children(self) -> bool:
        return self.frame is not None

class TxRefSyntheticProvider:
    def __init__(self, valobj: lldb.SBValue, internal_dict):
        self.valobj = valobj
        self.update()

    def num_children(self) -> int:
        return 4

    def get_child_index(self, name) -> int:
        return {
            'data_copy': 0,
            'frame_copy': 1,
            'detach': 2,
            'tx': 3,
        }.get(name, -1)

    def get_child_at_index(self, index):
        if index == 0:
            return self.data
        if index == 1:
            return self.frame
        elif index == 2:
            return self.detach
        elif index == 3:
            return self.tx
        else:
            raise ValueError()

    def update(self):
        self.data = None
        self.frame = None

        target: lldb.SBTarget = self.valobj.target

        self.ty: lldb.SBType = self.valobj.type.template_args[0]
        self.detach: lldb.SBValue = self.valobj.GetChildMemberWithName('detach')
        self.tx: lldb.SBValue = self.valobj.GetChildMemberWithName('tx')
        try:
            expr = f'hornpipedbg_lookup_transaction_copy({get_expression_path(self.tx)}.raw, {get_expression_path(self.detach)}.raw)'
            self.frame = self.valobj.CreateValueFromExpression("frame_copy", expr)
        except Exception as e:
            print(e)
            return
        
        if self.frame is not None and self.frame.GetValueAsUnsigned() != 0:
            frame_val = self.frame.Dereference()
            data_offset = frame_val.type.fields[-1].byte_offset
            self.data = frame_val.CreateChildAtOffset('data_copy', data_offset, self.ty)
            assert self.data is not None
            assert self.data.IsValid()

    def has_children(self) -> bool:
        return True

